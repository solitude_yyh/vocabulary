const app = getApp();
var inputVal = '';
var msgList = [];
var windowWidth = wx.getSystemInfoSync().windowWidth;
var windowHeight = wx.getSystemInfoSync().windowHeight;
var keyHeight = 0;

let socketOpen = false;
let socketMsgQueue = [];
let lineCount = Math.floor(windowWidth / 16) - 6;
let curAnsCount = 0;

function initData(that) {
  inputVal = '';
  msgList = [{
    speaker: 'server',
    contentType: 'text',
    content: '你好，我是欢乐单词姐，快来和我斗单词吧？'
  }, ]
  that.setData({
    msgList,
    inputVal
  })
}

Page({
  /**
   * 页面的初始数据
   */
  data: {
    scrollHeight: '100vh',
    inputBottom: 0
  },

  async init() {
    wx.setNavigationBarTitle({
        title: '欢乐斗单词聊天室',
    })

    if (!this.control.isLogin) {
      wx.showToast({
          title: '您还未登录哦~',
          icon: 'none',
          duration: 1500,
      })
      if (app.globalData.tryingLogin) {
          let _this = this
          this.control.loginTimer = setInterval(function () {
              if (!app.globalData.tryingLogin) {
                  _this.control.pageHide = true
                  _this.onShow()
                  clearInterval(_this.control.loginTimer)
              }
          }, 200)
      }
      return
  }
  this.getSingleWBData()
  this.initLearnData()
},



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.init();
    initData(this);
    this.setData({
      cusHeadIcon: "../static/images/tab-learn-007BFF.png",
    });
  },


  /**
   * 生命周期函数--监听页面显示
   */
  
  onHide: function () {
    wx.closeSocket()
    wx.onSocketClose((result) => {
      console.log("socket关闭成功");
      wx.showToast({
        icon: 'none',
        title: '会话关闭成功',
        duration: 500
      })
    })
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 获取聚焦
   */
  focus: function (e) {
    let res = wx.getSystemInfoSync()
    let navBarHeight = res.statusBarHeight + 44 //顶部状态栏+顶部导航，大部分机型默认44px
    keyHeight = e.detail.height - navBarHeight;
    if (keyHeight < 0) {
      keyHeight = 0
    }
    this.setData({
      scrollHeight: (windowHeight - keyHeight) + 'px'
    });
    this.setData({
      toView: 'msg-' + (msgList.length - 1),
      inputBottom: (keyHeight) + 'px'
    })
  },

  //失去聚焦(软键盘消失)
  blur: function (e) {
    this.setData({
      scrollHeight: '100vh',
      inputBottom: 0
    })
    this.setData({
      toView: 'msg-' + (msgList.length - 1)
    })

  },

  /**
   * 发送点击监听
   */
  sendClick: function (e) {
    sendSocketMessage(e.detail.value)
    msgList.push({
      speaker: 'customer',
      contentType: 'text',
      content: e.detail.value
    })
    msgList.push({
      speaker: 'server',
      contentType: 'text',
      content: ''
    })
    inputVal = '';
    this.setData({
      msgList,
      inputVal
    });
  },

  /**
   * 退回上一页
   */
  toBackClick: function () {
    wx.navigateBack({})
  }

})